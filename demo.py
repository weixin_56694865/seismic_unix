import subprocess
import os

# 确保 SU 的路径在环境变量中
# os.environ["PATH"] += ":/path/to/seismic_unix"

# 定义命令
command = "suplane | suxwigb"

# 运行命令并捕获输出
try:
    output = subprocess.check_output(command, shell=True, text=True, stderr=subprocess.STDOUT)
    print("命令输出：")
    print(output)
except subprocess.CalledProcessError as e:
    print(f"命令运行失败，返回码：{e.returncode}")
    print(f"错误信息：{e.output}")